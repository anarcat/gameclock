Playing chess with friends in the Pampa Humida, I couldn't find a
proper way to teach my friends on how to play quickly enough so that
the game wouldn't become boring as hell. So I found this [web page][1]
with a simple chess clock on it. Written in javascript, it wasn't
exactly reliable or fast, and required me to have network access
(unless I saved the page to my machine, but anyways). Also, the web
browser overhead was too much for my taste.

 [1]: http://smashhatter.com/chess/chesstimer/chessTimer.html

So I started looking around for software to fulfill my needs. The only
clock I could find was [Ghronos][2] but it was Java-based and (so?) I
couldn't get it to run natively on my machine (it would run in a
browser, but then I would be back to square one).

 [2]: http://ghronos.sourceforge.net/

So I opened up Emacs and started hacking at a pygtk program, because I
like Python and I found that GTK looked decent enough I wanted to
learn it. Within a few hours, I got a basic version working (0.1) that
was just a countdown. A few hours more and I got fischer delays
implemented (0.2). (That took around five hours according to the
changelog.)

Then I went to debconf8 in Mar del Plata and tried real hard (okay, I
didn't try at all) to keep myself from working on the software and
follow the conference, and failed, so I polished the interface and
implemented more features: a reset handler, control over the initial
time (duh!), colors, etc. Now I think it's a pretty decent clock,
still lacking some features, but it's been fun anyways.

Now I've got a 1.0 version which seems pretty mature to me
anyways. And only now does a friend of mine point me to an already
existing "chessclock" program, written in [ruby][3], and much to my
demise: it works well and looks pretty good! Still, I had fun writing
my version of the software, it's just unfortunate to duplicate work
like this. I have therefore started integrating the features from
"chessclock" missing in my program, namely: fullscreen mode, shift
keys to change turns and turn counting. I consider now both
applications to be roughly equivalent.

 [3]: http://gnomecoder.wordpress.com/chessclock/

I have now therefore renamed my software to pychessclock to avoid
confusion. Version 2.0 rewrites the gaming engine to support any
number of clocks and the package was therefore renamed again, this
time to gameclock.
