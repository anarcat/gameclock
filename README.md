A python Chess clock
====================

This is a simple game clock (mainly for chess now) that is designed to
be fast and simple to use.

Usage
-----

Starting it should be as simple as just starting the script from the shell:

    ./scripts/gameclock

If that doesn't work, use the python interpreter directly:

    python scripts/gameclock

Most of the controls are done through the keyboard, see the features
list for the shortcuts.

Requirements
------------

 * Python 2.3 or greater
 * PyGTK 2.0 or greater

License
-------

The chess clock is free software, see the [LICENSE](LICENSE) file for more
information.

Features
--------

Arbitrary number of clocks. Can act as a chronometer when only one
clock is enabled

Game modes:

 * Class (Times per move) - after move, reset counter
 * Blitz - simple countdown
 * Fischer - add N seconds after player move
 * Hourglass - time used by a player is given to the other(s)

Key controls (buttons):

 * space - start game, end turn
 * shift keys end turn for the right or left player, as appropriate
 * control-n - start a new game
 * control-p - pause
 * control-q - quit
 * control-f - toggle fullscreen

Other controls:

 * starting left or right
 * setting initial time
 * miliseconds display

Bugs, missing features and project updates are reported here:

https://0xacab.org/anarcat/gameclock

Missing features
----------------

The following timers are not implemented:

 * Multiple mouse support: have users hit a mouse instead of the
   keyboard. Seems like this is a limitation in PyGTK:
   https://bugzilla.gnome.org/show_bug.cgi?id=654215 See also bf2fd9eb
   and 400cf756

 * Separate settings for each user: for example, one user may have
   more time than the other

 * Allow changing the starting player when N > 2

 * Bronstein timer: roughly similar to Fischer, but instead of
   modifying the clock, we wait N seconds before starting player
   counter.

 * Delay timer: "After N moves, counters reset". This is similar to
   the endgame scenarios in go. Note that the "boardgame" clock is a
   special case of this - after 1 move the clock resets...

 * "Canadian" byoyomi go timers:
 
     > A byo-yomi overtime style traditionally used for go. A clock
     > counts down until its time runs out. It then enters
     > overtime. Once in overtime the player has a specific number of
     > moves to make. If the player makes the specified number of
     > moves before time runs out the clock's time reverts to the top
     > of the overtime. If the player does not make the specified
     > number of moves the game ends and the player loses. The number
     > of required moves, the length of the overtime, and which clocks
     > the overtime applies to are configurable.

   See also: http://en.wikipedia.org/wiki/Go_(board_game)#Time_control
             https://en.wikipedia.org/wiki/Byoyomi
             http://pages.infinit.net/steven/byoyomi.htm

Known issues
------------

Problems with the audio driver may hang the game, see [this description][].

[this description]: http://web.archive.org/web/20160902213333/https://redmine.koumbit.net/issues/10438
