#!/usr/bin/python

import unittest
import sys
import os

sys.path.insert(0, os.path.dirname(__file__))

suite = unittest.TestLoader().discover('tests')
unittest.TextTestRunner().run(suite)
