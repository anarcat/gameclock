#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
setup.py for Gameclock
"""
import os
import sys
from setuptools import setup
from gameclock import __version__

from distutils import cmd
from distutils.command.build import build as _build
import msgfmt


# stolen from deluge-1.3.3 (GPL3)
class build_trans(cmd.Command):
    description = 'Compile .po files into .mo files'

    user_options = [
        ('build-lib', None, "lib build folder")
    ]

    def initialize_options(self):
        self.build_lib = None

    def finalize_options(self):
        self.set_undefined_options('build', ('build_lib', 'build_lib'))

    def run(self):
        po_dir = os.path.join(os.path.dirname(__file__), 'po/')

        print('Compiling po files from %s...' % po_dir),
        for path, names, filenames in os.walk(po_dir):
            for f in filenames:
                uptoDate = False
                if f.endswith('.po'):
                    lang = f[:len(f) - 3]
                    src = os.path.join(path, f)
                    dest_path = os.path.join(self.build_lib,
                                             'gameclock',
                                             'po', lang,
                                             'LC_MESSAGES')
                    dest = os.path.join(dest_path, 'gameclock.mo')
                    if not os.path.exists(dest_path):
                        os.makedirs(dest_path)
                    if not os.path.exists(dest):
                        sys.stdout.write('%s, ' % lang)
                        sys.stdout.flush()
                        msgfmt.make(src, dest)
                    else:
                        src_mtime = os.stat(src)[8]
                        dest_mtime = os.stat(dest)[8]
                        if src_mtime > dest_mtime:
                            sys.stdout.write('%s, ' % lang)
                            sys.stdout.flush()
                            msgfmt.make(src, dest)
                        else:
                            uptoDate = True
                            
        if uptoDate:
            sys.stdout.write(' po files already upto date.  ')
        sys.stdout.write('\b\b \nFinished compiling translation files. \n')


class build(_build):
    sub_commands = [('build_trans', None)] + _build.sub_commands

    def run(self):
        # Run all sub-commands (at least those that need to be run)
        _build.run(self)


cmdclass = {
    'build': build,
    'build_trans': build_trans,
}

setup(
    name="gameclock",
    version=__version__,
    description="The Gameclock",
    author="Antoine Beaupré",
    author_email="anarcat@orangeseeds.org",
    license='GPLv3',
    url="https://0xacab.org/anarcat/gameclock/",
    cmdclass=cmdclass,
    packages=["gameclock"],
    scripts=["scripts/gameclock"],
    data_files=[('share/pixmaps', ['gameclock.xpm']),
                ('share/icons/hicolor/scalable', ['gameclock.svg']),
                ('share/applications', ['gameclock.desktop'])],
    keywords='games clock boardgame timer chess fischer go hourglass',
    classifiers=[
        'Development Status :: 6 - Mature',
        'Environment :: X11 Applications :: GTK',
        'Intended Audience :: End Users/Desktop',
        'License :: OSI Approved :: GNU Lesser General Public License v3 or later (LGPLv3+)',
        'Natural Language :: English',
        'Natural Language :: French',
        'Operating System :: POSIX :: Linux',
        'Operating System :: Unix',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.3',
        'Programming Language :: Python :: 2.4',
        'Programming Language :: Python :: 2.5',
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 2 :: Only',
        'Topic :: Games/Entertainment',
        'Topic :: Games/Entertainment :: Board Games',
        ],
    )
